* NRT via a Score object

#+name: pwd
#+BEGIN_SRC shell
echo $PWD
#+END_SRC

#+RESULTS: pwd
: /home/mis/src/_SAT/audio-protos/sc-protos


#+name: path
#+BEGIN_SRC sclang :var pwd=(org-sbe pwd)
thisProcess.nowExecutingPath=pwd;
~path = thisProcess.nowExecutingPath;
#+END_SRC

#+name: wDefFile
#+BEGIN_SRC sclang
  SynthDef("NRTsine", { |out, freq = 440|
      Out.ar(out,
           SinOsc.ar(freq, 0, 0.2)
      )
  }).writeDefFile;
#+END_SRC

#+name:satieSupernovaConfig
#+BEGIN_SRC sclang
  s = Server.supernova.local;
  ~satieConfiguration = SatieConfiguration.new(s, [\stereoListener]);
  ~satie = Satie.new(~satieConfiguration);
  ~satie.boot();
  ~satie.doneCb = {
    ~satie.quit();
    ~satie.generatedSynthDefs.postln;
    x = [
      [5.0, [ \s_new, \testtone, 1000, 0, 0,  \sfreq, 1413, \gainDB, -30 ]],
      [5.1, [ \s_new, \testtone, 1001, 0, 0,  \sfreq, 712 , \gainDB, -30]],
      [5.2, [ \s_new, \testtone, 1002, 0, 0,  \sfreq, 417 , \gainDB, -30]],
      [5.3, [ \s_new, \testtone, 1003, 0, 0,  \sfreq, 1238, \gainDB, -30 ]],
      [5.4, [ \s_new, \testtone, 1004, 0, 0,  \sfreq, 996 , \gainDB, -30]],
      [5.5, [ \s_new, \testtone, 1005, 0, 0,  \sfreq, 1320, \gainDB, -30 ]],
      [5.6, [ \s_new, \testtone, 1006, 0, 0,  \sfreq, 864 , \gainDB, -30]],
      [5.7, [ \s_new, \testtone, 1007, 0, 0,  \sfreq, 1033, \gainDB, -30 ]],
      [5.8, [ \s_new, \testtone, 1008, 0, 0,  \sfreq, 1693, \gainDB, -30 ]],
      [5.9, [ \s_new, \testtone, 1009, 0, 0,  \sfreq, 410 , \gainDB, -30]],
      [7.0, [ \s_new, \testtone, 1010, 0, 0,  \sfreq, 1349, \gainDB, -30 ]],
      [7.1, [ \s_new, \testtone, 1011, 0, 0,  \sfreq, 1449, \gainDB, -30 ]],
      [7.2, [ \s_new, \testtone, 1012, 0, 0,  \sfreq, 1603, \gainDB, -30 ]],
      [7.3, [ \s_new, \testtone, 1013, 0, 0,  \sfreq, 333 , \gainDB, -30]],
      [7.4, [ \s_new, \testtone, 1014, 0, 0,  \sfreq, 678 , \gainDB, -30]],
      [7.5, [ \s_new, \testtone, 1015, 0, 0,  \sfreq, 503 , \gainDB, -30]],
      [7.6, [ \s_new, \testtone, 1016, 0, 0,  \sfreq, 820 , \gainDB, -30]],
      [7.7, [ \s_new, \testtone, 1017, 0, 0,  \sfreq, 1599, \gainDB, -30 ]],
      [7.8, [ \s_new, \testtone, 1018, 0, 0,  \sfreq, 968 , \gainDB, -30]],
      [7.9, [ \s_new, \testtone, 1019, 0, 0,  \sfreq, 1347, \gainDB, -30 ]],
      [9.0, [\c_set, 0, 0]]
    ];
    Score.write(x, thisProcess.nowExecutingPath++"/score-satie2.osc");
    "scsynth -N %/score-test.osc _ %/satie-test2.wav 48000 WAV int16 -o 1".format(~path, ~path).unixCmd
  };

#+END_SRC

#+name: ex_score
#+BEGIN_SRC sclang :var pwd=(org-sbe pwd) :noweb yes
x = [
[0.0, [ \s_new, \NRTsine, 1000, 0, 0,  \freq, 1413 ]],
[0.1, [ \s_new, \NRTsine, 1001, 0, 0,  \freq, 712 ]],
[0.2, [ \s_new, \NRTsine, 1002, 0, 0,  \freq, 417 ]],
[0.3, [ \s_new, \NRTsine, 1003, 0, 0,  \freq, 1238 ]],
[0.4, [ \s_new, \NRTsine, 1004, 0, 0,  \freq, 996 ]],
[0.5, [ \s_new, \NRTsine, 1005, 0, 0,  \freq, 1320 ]],
[0.6, [ \s_new, \NRTsine, 1006, 0, 0,  \freq, 864 ]],
[0.7, [ \s_new, \NRTsine, 1007, 0, 0,  \freq, 1033 ]],
[0.8, [ \s_new, \NRTsine, 1008, 0, 0,  \freq, 1693 ]],
[0.9, [ \s_new, \NRTsine, 1009, 0, 0,  \freq, 410 ]],
[1.0, [ \s_new, \NRTsine, 1010, 0, 0,  \freq, 1349 ]],
[1.1, [ \s_new, \NRTsine, 1011, 0, 0,  \freq, 1449 ]],
[1.2, [ \s_new, \NRTsine, 1012, 0, 0,  \freq, 1603 ]],
[1.3, [ \s_new, \NRTsine, 1013, 0, 0,  \freq, 333 ]],
[1.4, [ \s_new, \NRTsine, 1014, 0, 0,  \freq, 678 ]],
[1.5, [ \s_new, \NRTsine, 1015, 0, 0,  \freq, 503 ]],
[1.6, [ \s_new, \NRTsine, 1016, 0, 0,  \freq, 820 ]],
[1.7, [ \s_new, \NRTsine, 1017, 0, 0,  \freq, 1599 ]],
[1.8, [ \s_new, \NRTsine, 1018, 0, 0,  \freq, 968 ]],
[1.9, [ \s_new, \NRTsine, 1019, 0, 0,  \freq, 1347 ]],
[3.0, [\c_set, 0, 0]]
];
Score.write(x, thisProcess.nowExecutingPath++"/score-test.osc");
//"scsynth -N %/score-test.osc _ %/test.wav 48000 WAV int16 -o 1".format(~path, ~path).unixCmd
#+END_SRC

#+name: satie_score
#+BEGIN_SRC sclang :var pwd=(org-sbe pwd) :noweb yes
x = [
[5.0, [ \s_new, \testtone, 1000, 0, 0,  \sfreq, 1413, \gainDB, -30 ]],
[5.1, [ \s_new, \testtone, 1001, 0, 0,  \sfreq, 712 , \gainDB, -30]],
[5.2, [ \s_new, \testtone, 1002, 0, 0,  \sfreq, 417 , \gainDB, -30]],
[5.3, [ \s_new, \testtone, 1003, 0, 0,  \sfreq, 1238, \gainDB, -30 ]],
[5.4, [ \s_new, \testtone, 1004, 0, 0,  \sfreq, 996 , \gainDB, -30]],
[5.5, [ \s_new, \testtone, 1005, 0, 0,  \sfreq, 1320, \gainDB, -30 ]],
[5.6, [ \s_new, \testtone, 1006, 0, 0,  \sfreq, 864 , \gainDB, -30]],
[5.7, [ \s_new, \testtone, 1007, 0, 0,  \sfreq, 1033, \gainDB, -30 ]],
[5.8, [ \s_new, \testtone, 1008, 0, 0,  \sfreq, 1693, \gainDB, -30 ]],
[5.9, [ \s_new, \testtone, 1009, 0, 0,  \sfreq, 410 , \gainDB, -30]],
[7.0, [ \s_new, \testtone, 1010, 0, 0,  \sfreq, 1349, \gainDB, -30 ]],
[7.1, [ \s_new, \testtone, 1011, 0, 0,  \sfreq, 1449, \gainDB, -30 ]],
[7.2, [ \s_new, \testtone, 1012, 0, 0,  \sfreq, 1603, \gainDB, -30 ]],
[7.3, [ \s_new, \testtone, 1013, 0, 0,  \sfreq, 333 , \gainDB, -30]],
[7.4, [ \s_new, \testtone, 1014, 0, 0,  \sfreq, 678 , \gainDB, -30]],
[7.5, [ \s_new, \testtone, 1015, 0, 0,  \sfreq, 503 , \gainDB, -30]],
[7.6, [ \s_new, \testtone, 1016, 0, 0,  \sfreq, 820 , \gainDB, -30]],
[7.7, [ \s_new, \testtone, 1017, 0, 0,  \sfreq, 1599, \gainDB, -30 ]],
[7.8, [ \s_new, \testtone, 1018, 0, 0,  \sfreq, 968 , \gainDB, -30]],
[7.9, [ \s_new, \testtone, 1019, 0, 0,  \sfreq, 1347, \gainDB, -30 ]],
[9.0, [\c_set, 0, 0]]
];
Score.write(x, thisProcess.nowExecutingPath++"/score-satie.osc");
//"scsynth -N %/score-test.osc _ %/test.wav 48000 WAV int16 -o 1".format(~path, ~path).unixCmd
#+END_SRC


#+name: ex_output
#+BEGIN_SRC sclang
"scsynth -N %/score-test.osc _ %/test.wav 48000 WAV int16 -o 1".format(~path, ~path).unixCmd
#+END_SRC

#+name: satie_output
#+BEGIN_SRC sclang
"scsynth -N %/score-test.osc _ %/satie-test2.wav 48000 WAV int16 -o 1".format(~path, ~path).unixCmd
#+END_SRC

#+name: ex_nrt
#+BEGIN_SRC sclang :noweb yes :var pwd=(org-sbe pwd)
<<path>>
<<satieSupernovaConfig>>
<<wDefFile>>
<<ex_score>>
<<ex_output>>
#+END_SRC

#+name: satie-nrt
#+BEGIN_SRC sclang :noweb yes :var pwd=(org-sbe pwd) :tangle satie-nrt.scd
<<path>>
<<satieSupernovaConfig>>
//<<wDefFile>>
//<<satie_score>>
//<<satie_output>>
#+END_SRC
