#!/usr/bin/python3
import argparse
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as ssig
import soundfile as sf
import utility as U

parser = argparse.ArgumentParser(
    description='Compute an Impulse Response deconvolving a recorded ESS '
    'signal with it''s associeted filter')
parser.add_argument('-r', required=True, metavar='',
                    help='Recorded File of an ESS')
parser.add_argument('-f', required=True, metavar='', help='Inverse Filter')
parser.add_argument('-i', required=True, metavar='',
                    help='Impulse response FileName')
parser.add_argument(
    '-p',
    help='Plots IR',
    action='store_true',)


args = parser.parse_args()


def farina_deconv(recorded_ess, filter_filename, ir_filename, plot):
    # Load data
    output, f = sf.read(recorded_ess)
    numb_channels = np.shape(output)[1]
    filter = np.load(filter_filename)
    # Zeropad
    z = np.zeros(np.shape(output)[0])
    z[0:np.shape(filter)[0]] = filter
    filter = z
    IR = np.zeros(np.shape(output))
    for i in range(0, numb_channels):
        # Compute the IR
        IR[:, i] = ssig.fftconvolve(output[:, i], filter, mode='same')
    # Normalization
    IR = IR / (np.max(IR))
    sf.write(ir_filename, IR, f, 'PCM_24')

    if plot:
        t = np.linspace(0, np.shape(IR)[0] / f, np.shape(IR)[0])
        plt.figure()
        plt.plot(t, U.db_amp(IR))
        plt.xlabel('Time (s)')
        plt.ylabel('Amplitud (db)')
        plt.show()


if __name__ == "__main__":
    farina_deconv(args.r, args.f, args.i, int(args.p))
