#!/bin/bash

IRFILE=$1
IRDIR=$(echo $PWD)
HEADER="/cd $IRDIR"
#                       in  out   partition    maxsize    density
#       --------------------------------------------------------
JACKIO="/convolver/new   1  2     256           204800      1.0\n
#              num   port name     connect to
# ----------------------------------------------
/input/name     1     input          PulseAudio\ JACK\ Sink:front-left
/output/name    1      L    system:playback_1
/output/name    2      R    system:playback_2"

IR="\n
#               in out  gain  delay  offset  length  chan      file
# ---------------------------------------------------------------------
/impulse/read   1   1   1   0     0        0       1        $1
/impulse/read   1   2   1   0     0        0       1        $1
"

echo -e "$HEADER \n$JACKIO \n$IR" > cur_ir.conf
cat cur_ir.conf
jconvolver -s default cur_ir.conf
