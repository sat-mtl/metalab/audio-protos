#!/bin/bash

IRFILE=$1
IRDIR=$(echo $PWD)
HEADER="/cd $IRDIR"
#                       in  out   partition    maxsize    density
#       --------------------------------------------------------
JACKIO="/convolver/new   1  16     256           204800      1.0\n
#              num   port name     connect to
# ----------------------------------------------
/input/name     1     input

/output/name    1     Out.1           supernova:input_1
/output/name    2     Out.2           supernova:input_2
/output/name    3     Out.3           supernova:input_3
/output/name    4     Out.4           supernova:input_4
/output/name    5     Out.5           supernova:input_5
/output/name    6     Out.6           supernova:input_6
/output/name    7     Out.7           supernova:input_7
/output/name    8     Out.8           supernova:input_8
/output/name    9     Out.9           supernova:input_9
/output/name    10    Out.10          supernova:input_10
/output/name    11    Out.11          supernova:input_11
/output/name    12    Out.12          supernova:input_12
/output/name    13    Out.13          supernova:input_13
/output/name    14    Out.14          supernova:input_14
/output/name    15    Out.15          supernova:input_15
/output/name    16    Out.16          supernova:input_16
"

IR="\n
#               in out  gain  delay  offset  length  chan      file
# ---------------------------------------------------------------------
/impulse/read   1   1   1   0     0        0       1        $1
/impulse/read   1   2   1   0     0        0       2        $1
/impulse/read   1   3   1   0     0        0       3        $1
/impulse/read   1   4   1   0     0        0       4        $1
/impulse/read   1   5   1   0     0        0       5        $1
/impulse/read   1   6   1   0     0        0       6        $1
/impulse/read   1   7   1   0     0        0       7        $1
/impulse/read   1   8   1   0     0        0       8        $1
/impulse/read   1   9   1   0     0        0       9        $1
/impulse/read   1   10  1   0     0        0      10        $1
/impulse/read   1   11  1   0     0        0      11        $1
/impulse/read   1   12  1   0     0        0      12        $1
/impulse/read   1   13  1   0     0        0      13        $1
/impulse/read   1   14  1   0     0        0      14        $1
/impulse/read   1   15  1   0     0        0      15        $1
/impulse/read   1   16  1   0     0        0      16       $1
"

echo -e "$HEADER \n$JACKIO \n$IR" > cur_ir.conf
cat cur_ir.conf
jconvolver -s default cur_ir.conf
