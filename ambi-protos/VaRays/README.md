# Ambisonic vonvolution reverb

General pipeline to re-incode HOA files to binaural:

```
audiofile.wav --> jconvolver --> SATIE
```

Steps to achieve this:

- decide which order you need to play and provide appropriate IR
- according to the chosen order use corresponding script:
  - 2nd order [jc\_ambi2\_start.sh](jc_ambi2_start.sh)
  - 3rd order [jc\_ambi3\_start.sh](jc_ambi3_start.sh)
- start jack
- start SuperCollider with the corresponding project file (You need SATIE installed with SC-HOA requirements):
  - 2nd order: [ambi2\_to\_binaural.scd](ambi2_to_binaural.scd)
  - 3rd order: [ambi3\_to\_binaural.scd](ambi2_to_binaural.scd)
- execute the stuff between outer `()`
- open a terminal and execute the chosen script:
  ``` shell
  ./jc_xxxx.sh IR_file.wav
  ```
- watch out for any errors reported (normally it will echo the config to terminal and may report a harmless message of this kind `Line 2: partition size adjusted to 4096.`)
- observe that supernova has created appropriate number of input channels
- open another terminal (or another window in `terminator` or `byobu`)
- play a mono file and connect it to the `jconvolver:input` port in jack:
  `mplayer -ao jack:port=jconvolver female.wav`

If you want to record the result, execute `s.makeGui` in your SuperCollider instance and use the record button. Watch the `PostBuffer` for information on its name and location.

Note: all this can be done on the `nyx` machine (Cube/haptic2000), this repo is available in `/home/metalab/src` (it may need a fetch and pull and whatnot).
