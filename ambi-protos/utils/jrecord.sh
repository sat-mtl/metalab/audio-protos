#!/bin/bash

LOCATIONS=(basement_doors basement_floor 1st_floor 1.5_floor 2nd_floor 3rd_floor_platform 3rd_floor_top)

original_tty_state=$(stty -g)
trap "stty $original_tty_state" INT
ecasound -f:16,2,48000 -i resample,auto,/tmp/Sonic\ the\ Hedgehog-1959999348.mp3 -eadb:-20 -o jack,jconvolver:${LOCATIONS[6]}
