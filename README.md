# Audio prototypes

- [ambisonic IR/convolution](./ambi-protos) - some b-format IR recording and convolution
- [SuperCollider prototypes](./sc-protos) - Some SuperCollider prototypes
